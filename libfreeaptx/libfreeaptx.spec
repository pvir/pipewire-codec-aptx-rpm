%global majorversion 0
%global minorversion 1
%global microversion 1

%global baserelease 1

# https://bugzilla.redhat.com/983606
%global _hardened_build 1

Name:           libfreeaptx
Summary:        Open Source aptX codec library and utilities
Version:        %{majorversion}.%{minorversion}.%{microversion}
Release:        %{baserelease}%{?dist}
License:        LGPLv2
URL:            https://github.com/iamthehorker/libfreeaptx/
Source0:        https://github.com/iamthehorker/libfreeaptx/archive/refs/tags/%{version}.tar.gz

BuildRequires:  gcc
BuildRequires:  make

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%package        tools
Summary:        %{name} encoder and decoder utilities
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description
This is Open Source implementation of Audio Processing Technology codec (aptX)
derived from ffmpeg 4.0 project and licensed under LGPLv2.1+. This codec is
mainly used in Bluetooth A2DP profile.

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%description    tools
The %{name}-tools package contains openaptxenc encoder and openaptxdec decoder
command-line utilities.

%prep
%autosetup

%build
%make_build STATIC_UTILITIES= ANAME= LDFLAGS="%{build_ldflags}" CFLAGS="%{optflags}"

%install
%make_install PREFIX="%{_prefix}" LIBDIR="%{_lib}"

%files
%license COPYING
%doc README
%{_libdir}/%{name}.so.%{majorversion}
%{_libdir}/%{name}.so.%{version}

%files devel
%{_libdir}/%{name}.so
%{_includedir}/freeaptx.h
%{_libdir}/pkgconfig/%{name}.pc

%files tools
%{_bindir}/freeaptxenc
%{_bindir}/freeaptxdec

%changelog
