%global majorversion 0
%global minorversion 3
%global microversion 36

%global spaversion   0.2

%global debug_package %{nil}

# For rpmdev-bumpspec and releng automation
%global baserelease 1

# https://bugzilla.redhat.com/983606
%global _hardened_build 1

# where/how to apply multilib hacks
%global multilib_archs x86_64 %{ix86} ppc64 ppc s390x s390 sparc64 sparcv9 ppc64le

Name:           pipewire-codec-bluez5-aptx
Summary:        PipeWire Bluetooth aptX codec plugin
Version:        %{majorversion}.%{minorversion}.%{microversion}
Release:        %{baserelease}%{?dist}
License:        MIT
URL:            https://pipewire.org/
Source0:        https://gitlab.freedesktop.org/pipewire/pipewire/-/archive/%{version}/pipewire-%{version}.tar.gz

BuildRequires:  meson >= 0.49.0
BuildRequires:  gcc
BuildRequires:  g++
BuildRequires:  pkgconfig
BuildRequires:  pkgconfig(dbus-1)
BuildRequires:  pkgconfig(bluez)
BuildRequires:  sbc-devel
BuildRequires:  libfreeaptx-devel

%description
PipeWire media server Bluetooth aptX codec plugin.

%prep
%autosetup -p1 -n pipewire-%{version}

%build
%__meson  -D auto_features=disabled -D examples=disabled \
          -D bluez5=enabled -D bluez5-codec-aptx=enabled \
          %{_vpath_srcdir} %{_vpath_builddir}
%__meson compile -C %{_vpath_builddir} spa-codec-bluez5-aptx

%install

mkdir -p %{buildroot}%{_libdir}/spa-%{spaversion}/bluez5
cp %{_vpath_builddir}/spa/plugins/bluez5/libspa-codec-bluez5-aptx.so \
   %{buildroot}%{_libdir}/spa-%{spaversion}/bluez5/

%files
%{_libdir}/spa-%{spaversion}/bluez5/libspa-codec-bluez5-aptx.so

%changelog
* Thu Sep 16 2021 Pauli Virtanen <pav@iki.fi> - 0.3.36-1
- Initial version
